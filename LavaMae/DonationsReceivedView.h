//
//  DonationsReceivedView.h
//  LavaMae
//
//  Created by Alexander Crosson on 6/13/15.
//  Copyright © 2015 Lava Mae. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DonationsReceivedView : UIView

@property (nonatomic, strong) UIView *view;
@property (nonatomic, strong) UILabel *descriptionLabel;
@property (nonatomic, strong) UILabel *amountLabel;

- (void)setAmount:(double)amount;

@end
