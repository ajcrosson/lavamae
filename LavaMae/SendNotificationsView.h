//
//  SendNotificationsView.h
//  LavaMae
//
//  Created by Alexander Crosson on 6/13/15.
//  Copyright © 2015 Lava Mae. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SendNotificationsViewDelegate <NSObject>

- (void)sendShowerNotification;
- (void)sendMoneyNotification;

@end

@interface SendNotificationsView : UIView

@property (nonatomic, strong) UIView *view;
@property (nonatomic, strong) UIButton *sendShowerNotificationButton;
@property (nonatomic, strong) UIButton *sendMoneyNotificationButton;

@property (nonatomic, weak) id <SendNotificationsViewDelegate> delegate;

@end
