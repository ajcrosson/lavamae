//
//  ViewController.h
//  LavaMae
//
//  Created by Alexander Crosson on 6/13/15.
//  Copyright (c) 2015 Lava Mae. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (strong, nonatomic) IBOutlet UITextField *locationTextField;
@property (strong, nonatomic) IBOutlet UITextField *nameTextField;
@property (strong, nonatomic) IBOutlet UILabel *counterLabel;

- (IBAction)incrementShowerCount:(id)sender;
- (IBAction)sendShowerNotification:(id)sender;


@end

