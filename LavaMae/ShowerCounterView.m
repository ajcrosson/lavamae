//
//  ShowerCounterView.m
//  LavaMae
//
//  Created by Alexander Crosson on 6/13/15.
//  Copyright (c) 2015 Lava Mae. All rights reserved.
//

#import "ShowerCounterView.h"

@implementation ShowerCounterView

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        
        // Base View
        self.view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        [self.view setBackgroundColor:[UIColor greenColor]];
        [self addSubview:self.view];
        
        // Counter Label
        self.counterLabel = [[UILabel alloc] initWithFrame:CGRectMake((self.view.frame.size.width-90) / 2, 10, 40, 40)];
        [self.counterLabel setFont:[UIFont fontWithName:@"Helvetica" size:40]];
        [self.counterLabel setText:@"0"];
        [self.view addSubview:self.counterLabel];
        
        // Increment Button
        self.incrementButton = [[UIButton alloc] initWithFrame:CGRectMake(self.counterLabel.frame.origin.x+self.counterLabel.frame.size.width+20, 10, 40, 40)];
        [self.incrementButton addTarget:self action:@selector(incrementShowerCount) forControlEvents:UIControlEventTouchDown];
        [self.incrementButton.titleLabel setFont:[UIFont fontWithName:@"Helvetica" size:40]];
        [self.incrementButton setTitle:@"+" forState:UIControlStateNormal];
        [self.view addSubview:self.incrementButton];
        
        // Description Label
        self.descriptionLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, self.incrementButton.frame.origin.y+self.incrementButton.frame.size.height, self.view.frame.size.width, 20)];
        [self.descriptionLabel setText:@"Showers Today"];
        [self.view addSubview:self.descriptionLabel];
        
    }
    
    return self;
}

- (void)incrementShowerCount {
    [self.delegate incrementShowerCount];
}

- (void)setCount:(NSInteger)count {
    [self.counterLabel setText:[NSString stringWithFormat:@"%ld",count]];
}

@end
