//
//  ShowerCounterView.h
//  LavaMae
//
//  Created by Alexander Crosson on 6/13/15.
//  Copyright (c) 2015 Lava Mae. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ShowerCounterViewDelegate <NSObject>

- (void)incrementShowerCount;

@end

@interface ShowerCounterView : UIView

@property (nonatomic, strong) UIView *view;
@property (nonatomic, strong) UILabel *counterLabel;
@property (nonatomic, strong) UILabel *descriptionLabel;
@property (nonatomic, strong) UIButton *incrementButton;

- (void)setCount:(NSInteger)count;

// Delegate

@property (nonatomic ,weak) id <ShowerCounterViewDelegate> delegate;

@end
