//
//  DonationsReceivedView.m
//  LavaMae
//
//  Created by Alexander Crosson on 6/13/15.
//  Copyright © 2015 Lava Mae. All rights reserved.
//

#import "DonationsReceivedView.h"

@implementation DonationsReceivedView

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {

        // Base View
        self.view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        [self.view setBackgroundColor:[UIColor redColor]];
        [self addSubview:self.view];
        
        // Description Label
        self.descriptionLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 70)];
        [self.descriptionLabel setText:@"Donations Received Today"];
        [self.view addSubview:self.descriptionLabel];
        
        // Amount Label
        self.amountLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 30)];
        [self.amountLabel setFont:[UIFont fontWithName:@"Helvetica" size:24]];
        [self.view addSubview:self.amountLabel];
        
    }
    
    return self;
}

- (void)setAmount:(double)amount {
    
    // Number Formatter used to Format to 2 decimal
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [formatter setMaximumFractionDigits:2];
    [formatter setRoundingMode: NSNumberFormatterRoundUp];
    
    // Set Amount Label
    [self.amountLabel setText:[formatter stringFromNumber:[NSNumber numberWithDouble:amount]]];
}

@end
