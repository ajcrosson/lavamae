//
//  ViewController.m
//  LavaMae
//
//  Created by Alexander Crosson on 6/13/15.
//  Copyright (c) 2015 Lava Mae. All rights reserved.
//

#import "ViewController.h"
#import "ShowerCounterView.h"
#import "DonationsReceivedView.h"
#import "SendNotificationsView.h"

@interface ViewController () <UIPickerViewDelegate,UIPickerViewDataSource>

//@property (nonatomic, strong) DonationsReceivedView *donationsReceivedView;
//@property (nonatomic, strong) SendNotificationsView *sendNotificationsView;
//@property (nonatomic, strong) ShowerCounterView *showerCounterView;
//@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) NSArray *pickerData;
@property (nonatomic, assign) NSInteger showerCount;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.showerCount = 0;
    double amountReceived = 2500.43;
    
    self.pickerData = @[@"Location Changed",@"90% of initial goal achieved. Almost there.",@"100% of initial goal achieved"];
//    self.pickerView.delegate = self;
//    self.pickerView.dataSource = self;
    
    
    
//    self.scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
//    [self.scrollView setContentSize:CGSizeMake(self.view.frame.size.width, self.view.frame.size.height+80)];
//    [self.view addSubview:self.scrollView];
//    
//    // Shower Counter View
//    self.showerCounterView = [[ShowerCounterView alloc] initWithFrame:CGRectMake(0, 64, self.view.frame.size.width, 100)];
//    self.showerCounterView.delegate = self;
//    [self.scrollView addSubview:self.showerCounterView];
//    [self.showerCounterView setCount:_showerCount];
//    
//    // Donations Received View
//    self.donationsReceivedView = [[DonationsReceivedView alloc] initWithFrame:CGRectMake(0, self.showerCounterView.frame.origin.y+self.showerCounterView.frame.size.height, self.view.frame.size.width, 100)];
//    [self.scrollView addSubview:self.donationsReceivedView];
//    [self.donationsReceivedView setAmount:amountReceived];
//    
//    // Send Notifications View
//    self.sendNotificationsView = [[SendNotificationsView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, 80)];
//    self.sendNotificationsView.delegate = self;
//    [self.scrollView addSubview:self.sendNotificationsView];
}

- (void)incrementShowerCount {
//    self.showerCount += 1;
//    [self.showerCounterView setCount:_showerCount];
}

- (void)sendShowerNotification {

}

#pragma mark - Picker View Delegate


//- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
//    return 1;
//}
//
//-(NSInteger)pickerView:(UIPickerView *)picker numberofRowsInComponent:(NSInteger)component{
//    
//    return [_pickerData count];
//    
//}
//
//- (NSString *)pickerView:(UIPickerView *)picker titleForRow:(NSInteger)row forComponent:(NSInteger)component{
//    
//    return [_pickerData objectAtIndex:row];
//    
//}

- (void)sendMoneyNotification {
    NSLog(@"Send Money Notification");
}

- (IBAction)incrementShowerCount:(id)sender {
}

- (IBAction)sendShowerNotification:(id)sender {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"TEST" message:@"SDSD" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
    [alert show];
    
    NSLog(@"Send Shower Notification");
    UILocalNotification *localNotification = [[UILocalNotification alloc] init];
    localNotification.alertTitle = @"Foo";
    localNotification.fireDate = [NSDate dateWithTimeIntervalSinceNow:5];
    [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
}
@end
