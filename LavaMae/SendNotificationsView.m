//
//  SendNotificationsView.m
//  LavaMae
//
//  Created by Alexander Crosson on 6/13/15.
//  Copyright © 2015 Lava Mae. All rights reserved.
//

#import "SendNotificationsView.h"

@implementation SendNotificationsView

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        
        // Base View
        self.view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        [self.view setBackgroundColor:[UIColor redColor]];
        [self addSubview:self.view];
        
        CGFloat buttonHeight = self.view.frame.size.height / 2;
        
        // Shower Notification Button
        self.sendShowerNotificationButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, buttonHeight)];
        [self.sendShowerNotificationButton setBackgroundColor:[UIColor orangeColor]];
        [self.sendShowerNotificationButton setTitle:@"Shower" forState:UIControlStateNormal];
        [self.sendShowerNotificationButton addTarget:self action:@selector(sendShowerNotification) forControlEvents:UIControlEventTouchDown];
        [self.view addSubview:self.sendShowerNotificationButton];
        
        // Money Notification Button
        self.sendMoneyNotificationButton = [[UIButton alloc] initWithFrame:CGRectMake(0, self.sendShowerNotificationButton.frame.origin.y+self.sendShowerNotificationButton.frame.size.height, self.view.frame.size.width, buttonHeight)];
        [self.sendMoneyNotificationButton setBackgroundColor:[UIColor greenColor]];
        [self.sendMoneyNotificationButton setTitle:@"Money" forState:UIControlStateNormal];
        [self.sendMoneyNotificationButton addTarget:self action:@selector(sendMoneyNotification) forControlEvents:UIControlEventTouchDown];
        [self.view addSubview:self.sendMoneyNotificationButton];
    }
    
    return self;
}

- (void)sendShowerNotification {
    NSLog(@">....");
    [self.delegate sendShowerNotification];
}

- (void)sendMoneyNotification {
    NSLog(@">....");
    [self.delegate sendMoneyNotification];
}

@end
