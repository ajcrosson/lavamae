//
//  GlanceController.swift
//  counters Extension
//
//  Created by Manuel Carrasco Molina on 13/06/15.
//  Copyright © 2015 Lava Mae. All rights reserved.
//

import WatchKit
import Foundation


class GlanceController: WKInterfaceController {

    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
      
      let size = CGSizeMake(100,100)
      UIGraphicsBeginImageContext(size);
//      let circle = UIBezierPath(arcCenter: CGPoint(x: 0, y: 0), radius: 100, startAngle: 0, endAngle: 200, clockwise: true)
      
      let path = CGPathCreateMutable()
      CGPathAddArc(path, nil, 0, 0, 100, 0, 180, true)
      
      
      
      let image = UIGraphicsGetImageFromCurrentImageContext();
      UIGraphicsEndImageContext();
  
      
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

}
